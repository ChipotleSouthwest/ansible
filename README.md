# Ansible playbooks
### IMPORTANT
This repository is a part of stack which I use to bootstrap, operate, store and version control all my work environment at various levels, such as:

1. [Operating system](https://gitlab.com/ChipotleSouthwest/kickstart)
2. [System infill (packages, parameters and so on)](https://gitlab.com/ChipotleSouthwest/ansible)
3. [Dotfiles](https://gitlab.com/ChipotleSouthwest/dotfiles)

Now this whole "project" is just the first version so any advice, ideas, suggestions or fixes are welcome. Feel free to open an issue!

This and all other repositories will be expanding as I use it.

You won't find any demonstration gifs, screenshots or videos here because I believe all of these things should be used, not watched. Also I don't think I am such good lector so I don't repeat any documentation here - you can google it by yourself.

### My  ansible playbooks
There stored my ansible playbooks I using in already installed system.

[system](https://gitlab.com/ChipotleSouthwest/ansible/-/blob/master/system.yaml) playbook for installation package managers (flatpak and homebrew), packages and make sudo passwordless for wheel group. Result can be used by any user.

[docker](https://gitlab.com/ChipotleSouthwest/ansible/-/blob/master/docker.yaml) playbook for installation all I need for docker to use. Decided to separate these tasks into a separate file because of there is one final result for all of them.

[user](https://gitlab.com/ChipotleSouthwest/ansible/-/blob/master/docker.yaml) playbook install and configure all user-specific stuff such as set zsh as default shell, install oh-my-zsh and its plugins, [tpm](https://github.com/tmux-plugins/tpm) and its plugins and finaly install my dotfiles. List of all this you can find inside.

[legacy.sh](https://gitlab.com/ChipotleSouthwest/ansible/-/blob/master/legacy.sh) is common bash installation script which I use because don't have any ideas how to migrate it into an ansible task.

I use it like this because it eliminates the need to create configuration files for ansible:  
`ansible-playbook  --connection=local -i localhost, --ask-become-pass *.yaml`

I know about the ability to create roles, but haven't investigated it yet. I also planing to store sensitive configs in Ansible Vault but later.
