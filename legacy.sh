#!/bin/bash
mkdir -p ~/applications
cd ~/applications

#kafka bins
KAFKA_REGEX='.*">(.*)\/<\/a>'
if [[ $(curl -s https://dlcdn.apache.org/kafka/ | grep '\[DIR\]' | tail -n 1) =~ $KAFKA_REGEX ]]
then
  KAFKA_VERSION="${BASH_REMATCH[1]}"
  wget https://dlcdn.apache.org/kafka/$KAFKA_VERSION/kafka_2.13-$KAFKA_VERSION.tgz
  tar -xzf kafka_2.13-$KAFKA_VERSION.tgz
  rm kafka_2.13-$KAFKA_VERSION.tgz
  mv kafka_2.13-$KAFKA_VERSION kafka
fi

#offsetexplorer
wget https://www.kafkatool.com/download2/offsetexplorer.sh
chmod 744 offsetexplorer.sh
./offsetexplorer.sh
rm offsetexplorer.sh

#font
curl -OL https://github.com/ryanoasis/nerd-fonts/releases/latest/download/FiraCode.tar.xz
mkdir FiraCode
tar xvf FiraCode.tar.xz -C FiraCode
rsync -a --relative FiraCode/./*.ttf ~/.local/share/fonts
fc-cache
rm -rf FiraCode FiraCode.tar.xz

